import { ICommand } from "wokcommands"
import {duration, reply} from "../../helpers/messages";
import {embeds} from "../../helpers/embeds";
import {distube} from "../../main";
import {VoiceBasedChannel} from "discord.js";
import {channelChecks} from "../../helpers/validationChecks";

export default {
    names: "filter",
    category: 'Music',
    description: 'Apply/remove music filter. Filters: 3d, bassboost, karaoke, nightcore, vaporwave', // Required for slash commands
    minArgs: 1,
    maxArgs: 1,
    expectedArgs: '<filter>',
    slash: 'both', // Create both a slash and legacy command
    testOnly: false, // Only register a slash command for the testing guilds
    options: [
        {
            name: 'filter', // Must be lower case
            description: 'The filter',
            required: true,
            type: 'STRING',
        },
    ],

    callback: ({ message, interaction, args }) => {
        if (!channelChecks.voiceChannelChecks(message, interaction)) return;

        let filters = ["3d", "bassboost", "karaoke", "nightcore", "vaporwave"]

        const errorEmbed = embeds.createErrorEmbed("Invalid filter. Known filters are: " + filters.join(", "))


        // message is provided for a legacy command
        if (message) {

            // Check arg
            if (!filters.includes(args[0])){
                reply.replyToMessage(message, errorEmbed,undefined, duration.ThirtySeconds,true)
                return;
            }

            // Check queue
            if (distube.getQueue(message) == undefined){
                reply.replyToMessage(message, embeds.createErrorEmbed("Nothing is playing"),undefined, duration.ThirtySeconds,true)
                return;
            }

            try{
                message.react("👌")
                const filter = distube.setFilter(message, args[0])
                let filterEmbed = embeds.createSimpleInfoEmbed("Filters applied","Current queue filter: " + (filter.join(", ") || "Off"))
                reply.replyToMessage(message, filterEmbed,undefined, duration.OneMinute, true)
            }
            catch(err){
                console.log(err)
                reply.replyToMessage(message, errorEmbed,undefined, duration.OneMinute, true)
            }
            return
        }
        // interaction is provided for slash commands

        // Check arg
        if (!filters.includes(args[0])){
            reply.replyToInteraction(interaction, errorEmbed,undefined, duration.ThirtySeconds)
            return;
        }

        // Check queue
        if (distube.getQueue(interaction) == undefined){
            reply.replyToInteraction(interaction, embeds.createErrorEmbed("Nothing is playing"),undefined, duration.ThirtySeconds)
            return;
        }

        try{
            const filter = distube.setFilter(interaction, args[0])
            let filterEmbed = embeds.createSimpleInfoEmbed("Filters applied","Current queue filter: " + (filter.join(", ") || "Off"))
            reply.replyToInteraction(interaction, filterEmbed,undefined, duration.TenSeconds)
        }
        catch(err){
            reply.replyToInteraction(interaction, errorEmbed,undefined, duration.OneMinute)
        }

    },
} as ICommand

