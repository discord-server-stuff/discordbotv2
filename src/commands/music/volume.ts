import { ICommand } from "wokcommands"
import {duration, reply} from "../../helpers/messages";
import {embeds} from "../../helpers/embeds";
import {distube} from "../../main";
import {VoiceBasedChannel} from "discord.js";
import {channelChecks} from "../../helpers/validationChecks";
import {parseNumber} from "distube";

export default {
    names: "volume",
    category: 'Music',
    description: 'Set the volume of the bot', // Required for slash commands
    aliases: ['vol'],
    minArgs: 1,
    maxArgs: 1,
    expectedArgs: '<volume>',
    slash: 'both', // Create both a slash and legacy command
    testOnly: false, // Only register a slash command for the testing guilds
    options: [
        {
            name: 'volume', // Must be lower case
            description: 'The volume must be greater or equal to 0',
            required: true,
            type: 'NUMBER',
        },
    ],

    callback: ({ message, interaction, args }) => {
        if (!channelChecks.voiceChannelChecks(message, interaction)) return;


        const errorEmbed = embeds.createErrorEmbed("Ey Einstein, Please give a volume amount it can be from 0 and up, however the higher above 100 the shittier the quality")


        // message is provided for a legacy command
        if (message) {

            // Check arg
            if (!Number.isInteger(parseInt(args[0]))){
                reply.replyToMessage(message, errorEmbed,undefined, duration.ThirtySeconds,true)
                return;
            }
            if (parseInt(args[0]) < 0){
                reply.replyToMessage(message, errorEmbed,undefined, duration.ThirtySeconds,true)
                return;
            }


            // Check queue
            if (distube.getQueue(message) == undefined){
                reply.replyToMessage(message, embeds.createErrorEmbed("Nothing is playing"),undefined, duration.ThirtySeconds,true)
                return;
            }

            try{
                message.react("👌")
                // @ts-ignore
                distube.setVolume(distube.getQueue(message),parseNumber(args[0]))
                let volumeEmbed = embeds.createSimpleInfoEmbed("Volume","Changed the volume to `" + args[0] + "`")
                reply.replyToMessage(message, volumeEmbed,undefined, duration.OneMinute, true)
            }
            catch(err){
                console.log(err)
                reply.replyToMessage(message, embeds.createErrorEmbed("Something went wrong"),undefined, duration.OneMinute, true)
            }
            return
        }
        // interaction is provided for slash commands

        // Check arg
        if (!Number.isInteger(parseInt(args[0]))){
            reply.replyToInteraction(interaction, errorEmbed,undefined, duration.ThirtySeconds,)
            return;
        }
        if (parseInt(args[0]) < 0){
            reply.replyToInteraction(interaction, errorEmbed,undefined, duration.ThirtySeconds)
            return;
        }

        // Check queue
        if (distube.getQueue(interaction) == undefined){
            reply.replyToInteraction(interaction, embeds.createErrorEmbed("Nothing is playing"),undefined, duration.ThirtySeconds)
            return;
        }

        try{
            let volumeEmbed = embeds.createSimpleInfoEmbed("Volume","Changed the volume to `" + args[0] + "`")
            // @ts-ignore
            distube.setVolume(distube.getQueue(interaction),parseNumber(args[0]))
            reply.replyToInteraction(interaction, volumeEmbed,undefined, duration.OneMinute)
        }
        catch(err){
            reply.replyToInteraction(interaction, embeds.createErrorEmbed("Something went wrong"),undefined, duration.OneMinute)
        }

    },
} as ICommand

