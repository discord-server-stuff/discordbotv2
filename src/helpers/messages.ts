import Discord, {GuildTextBasedChannel, MessageActionRow, MessageEmbed, TextChannel} from 'discord.js'
import * as Console from "console";
import {Queue} from "distube";

/**
 * Used for TTL of messages
 */
export enum duration {
    TenSeconds = 10000,
    ThirtySeconds = 30000,
    OneMinute = 60000,
    TenMinutes = 100000
}

/**
 *  Used to check if a message has been deleted by the user, such that when auto delete is called it doesn't crash, because it can't find the message
 */
export module deletes {
    const deletedMessages = new WeakSet<Discord.Message>();
    const deletedInteraction = new WeakSet<Discord.CommandInteraction>();

    /**
     * Might be usefull for tracking deleted messages
     * @param message message we are looking at
     */
    export function isMessageDeleted(message: Discord.Message) {
        return deletedMessages.has(message);
    }

    /**
     * * Might be useful for tracking deleted messages
     * @param message message we have deleted
     */
    export function markMessageAsDeleted(message: Discord.Message) {
        deletedMessages.add(message);
    }


    /**
     * Useful for deleting messages
     * @param message - Message which is to be deleted
     * @param timeToLive - Time before message is deleted
     */
    export function deleteMessageAfterTime(message: Discord.Message, timeToLive: number){
        setTimeout(() => {
            if (!isMessageDeleted(message)){
                message.delete().then(() => markMessageAsDeleted(message))
            }
        }, timeToLive)
    }

    export function deleteInteractionAfterTime(interaction: Discord.CommandInteraction, timeToLive: number){
        setTimeout(() => {
            interaction.deleteReply().catch(error => console.log("Couldn't delete: " + error))
        }, timeToLive)
    }

}

/**
 * Checks if a string is an url
 * @param str potential url
 */
export function validURL(str: string) {
    let pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return pattern.test(str);
}

/**
 * Allow for easy message sending of either a string or Embed, and and make it possible to delete reply and original message
 */
export module sendMessage {
    import isMessageDeleted = deletes.isMessageDeleted;
    import markMessageAsDeleted = deletes.markMessageAsDeleted;

    /**
     * A method for sending messages easily and streamlined
     * @param channel - What text channel it should be sent in
     * @param reply - The reply message
     * @param components extra components for the message
     * @param timeToLive? - Optional if the message needs to be deleted af some time written in milliseconds
     */
    export function sendMessageToChannel(channel: GuildTextBasedChannel | undefined, reply: string | MessageEmbed, components?: MessageActionRow[] | undefined, timeToLive?: number): void{
        //Sends message if the reply is an embed
        if (reply instanceof Discord.MessageEmbed) {
            // @ts-ignore
            channel.send({
                embeds: [reply],
                components:  components != undefined ? components : undefined
            }).then(newMessage => {
                if (timeToLive != null) {
                    setTimeout(() => {
                        if (!isMessageDeleted(newMessage)){
                            newMessage.delete().then(() => markMessageAsDeleted(newMessage))
                        }
                    }, timeToLive)
                }
            });
            return;
        }
        //Sends message if the reply is a string
        // @ts-ignore
        channel.send({
            content: reply,
        }).then(newMessage => {
            if (timeToLive != null) {
                setTimeout(() => {
                    if (!isMessageDeleted(newMessage)){
                        newMessage.delete().then(() => markMessageAsDeleted(newMessage))
                    }
                }, timeToLive)
            }
        });
        return;
    }
    /**
     * A method for sending messages easily and streamlined
     * @param queue - The queue object
     * @param reply - The reply message
     * @param components extra components for the message
     * @param timeToLive? - Optional if the message needs to be deleted af some time written in milliseconds
     */
    export function sendMessageUsingQueue(queue: Queue, reply: string | MessageEmbed, components?: MessageActionRow[] | undefined, timeToLive?: number): void{
        //Sends message if the reply is an embed
        let textChannel = queue.textChannel;
        if (reply instanceof Discord.MessageEmbed) {
            // @ts-ignore
            textChannel.send({
                embeds: [reply],
                components:  components != undefined ? components : undefined
            }).then(newMessage => {
                if (timeToLive != null) {
                    setTimeout(() => {
                        if (!isMessageDeleted(newMessage)){
                            newMessage.delete().then(() => markMessageAsDeleted(newMessage))
                        }
                    }, timeToLive)
                }
            });
            return;
        }
        //Sends message if the reply is a string
        // @ts-ignore
        textChannel.send({
            content: reply,
            components:  components != undefined ? components : undefined
        }).then(newMessage => {
            if (timeToLive != null) {
                setTimeout(() => {
                    if (!isMessageDeleted(newMessage)){
                        newMessage.delete().then(() => markMessageAsDeleted(newMessage))
                    }
                }, timeToLive)
            }
        });
        return;
    }

}

/**
 * Easy reply to a message as either a string or Embed, and make it possible to delete reply and original message
 */
export module reply {
    import isMessageDeleted = deletes.isMessageDeleted;
    import markMessageAsDeleted = deletes.markMessageAsDeleted;

    /**
     * A method for replying to messages easily and streamlined
     *
     * @param message - The message which we want to reply to
     * @param reply - The reply message
     * @param components extra components for the message
     * @param timeToLive - Optional if the message needs to be deleted af some time written in milliseconds
     * @param deleteOriginal - Optional If the original client message should be deleted aka. the Discord users message by default False
     */
    export async function replyToMessage(message: Discord.Message, reply: string | Discord.MessageEmbed, components?: MessageActionRow[] | undefined, timeToLive?: number, deleteOriginal?: boolean): Promise<Discord.Message> {
        let newMessage: Discord.Message
        //Sends message if the reply is an embed
        if (reply instanceof Discord.MessageEmbed) {
            newMessage = await message.reply({
                embeds: [reply],
                components:  components != undefined ? components : undefined
            })

            if (timeToLive != null) {
                if (deleteOriginal == null) {
                    deleteOriginal = false
                }
                setTimeout(() => {
                    if (!isMessageDeleted(newMessage)){
                        newMessage.delete().then(() => markMessageAsDeleted(newMessage))
                    }

                    if (deleteOriginal && !isMessageDeleted(message)){
                        message.delete().then(() => markMessageAsDeleted(message))
                    }
                }, timeToLive)
            }

            return newMessage;
        }
        //Sends message if the reply is a string
        newMessage = await message.reply({
            content: reply,
            components: components != undefined ? components : undefined
        })

        if (timeToLive != null) {
            if (deleteOriginal == null) {
                deleteOriginal = false
            }
            setTimeout(() => {
                if (!isMessageDeleted(newMessage)){
                    newMessage.delete().then(() => markMessageAsDeleted(newMessage))
                }

                if (deleteOriginal && !isMessageDeleted(message)){
                    message.delete().then(() => markMessageAsDeleted(message))
                }
            }, timeToLive)
        }

        return newMessage;
    }
    /**
     * A method for replying to interaction easily and streamlined
     *
     * @param interaction - The message which we want to reply to
     * @param reply - The reply message
     * @param components extra components for the message
     * @param timeToLive - Optional if the message needs to be deleted af some time written in milliseconds
     * @param ephemeral - If this respond needs to be hidden/only shown to the member who initiated the command
     */
    export function replyToInteraction(interaction: Discord.CommandInteraction, reply: string | Discord.MessageEmbed, components?: MessageActionRow[] | undefined, timeToLive?: number, ephemeral?: boolean): void {
        if (reply instanceof Discord.MessageEmbed) {
            interaction.reply({
                embeds: [reply],
                components:  components != undefined ? components : undefined,
                ephemeral: ephemeral
            }).then(newMessage => {
                setTimeout(() => {
                    interaction.deleteReply().catch(error => console.log("Couldn't delete: " + error))
                }, timeToLive)
            })
            return
        }
        interaction.reply({
            content: reply,
            components:  components != undefined ? components : undefined,
            ephemeral: ephemeral
        }).then(() => {
            setTimeout(() => {
                interaction.deleteReply().catch(error => console.log("Couldn't delete: " + error))
            }, timeToLive)
        })

    }
}

