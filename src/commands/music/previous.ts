import { ICommand } from "wokcommands"
import {duration, reply} from "../../helpers/messages";
import {embeds} from "../../helpers/embeds";
import {distube} from "../../main";
import {VoiceBasedChannel} from "discord.js";
import {channelChecks} from "../../helpers/validationChecks";

export default {
    names: "Previous",
    category: 'Music',
    description: 'Plays the previous song', // Required for slash commands
    aliases: ['prev'],
    slash: 'both', // Create both a slash and legacy command
    testOnly: false, // Only register a slash command for the testing guilds

    callback: ({ message, interaction, args }) => {
        if (!channelChecks.voiceChannelChecks(message, interaction)) return;

        const errorEmbed = embeds.createErrorEmbed("Something went wrong")


        // message is provided for a legacy command
        if (message) {
            if(distube.getQueue(message)?.previousSongs !== undefined){
                // @ts-ignore
                if(distube.getQueue(message)?.previousSongs.length < 1){
                    const errorEmbed = embeds.createErrorEmbed("No previous song")
                    reply.replyToMessage(message, errorEmbed,undefined, duration.OneMinute, true)
                    return;
                }
            }
            try {
                distube.previous(message)
            }catch(err){
                const errorEmbed = embeds.createErrorEmbed("Something went wrong")
                reply.replyToMessage(message, errorEmbed,undefined, duration.OneMinute, true)
            }
            return
        }

        // interaction is provided for slash commands
        if(distube.getQueue(interaction)?.previousSongs !== undefined){
            // @ts-ignore
            if(distube.getQueue(interaction)?.previousSongs.length < 1){
                const errorEmbed = embeds.createErrorEmbed("No previous song")
                reply.replyToInteraction(interaction, errorEmbed,undefined, duration.OneMinute)
            }
        }

        try {
            distube.previous(interaction)
        }catch(err){
            const errorEmbed = embeds.createErrorEmbed("Something went wrong")
            reply.replyToInteraction(interaction, errorEmbed,undefined, duration.OneMinute)
        }

    },
} as ICommand

