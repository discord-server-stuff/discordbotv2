import Discord, {GuildTextBasedChannel, MessageEmbed} from 'discord.js'
import {embeds} from "./embeds";
import {duration, reply} from "./messages";
// Check if correct argument

/**
 * Common checks to do before running a command
 */
export module channelChecks {
    /**
     * Command checks to do before running commands that use a voice channel. Returns true if all checks where successful
     * @param message - Message to check on
     * @param interaction - Interaction to check on
     */
    export function voiceChannelChecks(message: Discord.Message | undefined, interaction: Discord.CommandInteraction | undefined): boolean {
        if (interaction){
            const Embed = embeds.createSimpleIssue("You need to be in a channel to use this command")
            if (!interaction.member) return false;
            // @ts-ignore
            const inChannel = interaction.member.voice.channel;
            if (!inChannel){
                interaction.reply({embeds: [Embed], ephemeral: true})
                return false;
            }
            return true;
        }
        if (message){
            if (!message.member) return false;
            var inChannel = message.member.voice.channel

            //General checks
            if (!inChannel){
                const Embed = embeds.createSimpleIssue("You need to be in a channel to use this command")
                reply.replyToMessage(message, Embed,undefined, duration.ThirtySeconds)
                return false;
            }
            return true;
        }
        return false
    }
    /**
     * Command checks to do before running commands that use a text channel. Returns true if all checks where successful
     * @param message - Message to check on
     * @param interaction - Interaction to check on
     */
    export function textChannelChecks(message: Discord.Message | undefined, interaction: Discord.CommandInteraction | undefined): boolean {
        return true
    }

    /**
     * Helper function that can be used in a command to check that args are given
     * @param message - Message to check on
     * @param interaction - Interaction to check on
     * @param args
     * @param errorMsg
     */
    export function validArgsCheck(message: Discord.Message | undefined, interaction: Discord.CommandInteraction | undefined, args: [], errorMsg: string): boolean{
        let failed = false;
        if (args.length == 0) {
            failed = true;
        }
        if(interaction && failed){
            const Embed = embeds.createSimpleIssue(errorMsg)
            reply.replyToInteraction(interaction, Embed,undefined, duration.ThirtySeconds)
            return true;
        }
        if (message && failed){
            const Embed = embeds.createSimpleIssue(errorMsg)
            reply.replyToMessage(message, Embed,undefined, duration.ThirtySeconds)
            return false;
        }
        return true
    }
}