import mongoose from "mongoose";

/**
 * Streamlining interactions with the database
 */
export module databaseInteractions {
    /**
     * Will fetch a document/documents if any matches the given filter and model
     * @param model What type of data we are looking for
     * @param filter Filter for what documents to choose in the collection of models
     */
    export async function fetchDocuments(model: mongoose.Model<any>, filter: mongoose.FilterQuery<any>) {
        let docs
        docs = await model
            .find(filter)
            .catch(err => {
                console.error(err)
                return undefined
            })
        return docs
    }

    /**
     * Create a document using a given a document
     * @param document Documents to be created in the database
     */
    export async function createDocument(document: mongoose.Document): Promise<boolean> {
        document.save(await function (err) {
            if (err) {
                return false
            }
        });
        return true
    }
}