import { ICommand } from "wokcommands"
import {deletes, duration, reply} from "../helpers/messages";
import {embeds} from "../helpers/embeds";
import {joinVoiceChannel, VoiceConnectionStatus} from "@discordjs/voice";
import {channelChecks} from "../helpers/validationChecks";
import deleteMessageAfterTime = deletes.deleteMessageAfterTime;

export default {
    names: "join",
    category: 'General',
    description: 'Join the voice channel you are in',

    slash: 'both', // Create both a slash and legacy command
    testOnly: false, // Only register a slash command for the testing guilds

    callback: ({ message, interaction, client}) => {
        const Embed = embeds.createSimpleIssue("You need to be in a channel to use this command")
        if (!channelChecks.voiceChannelChecks(message, interaction)) return;
        //Handle legacy command
        if (message) {

            const connection = joinVoiceChannel({
                // @ts-ignore
                channelId: message.member.voice.channelId,
                // @ts-ignore
                guildId: message.guild.id,
                // @ts-ignore
                adapterCreator: message.channel.guild.voiceAdapterCreator,
            });
            message.react("👌")
            deleteMessageAfterTime(message, duration.ThirtySeconds)
            return
        }
        //Interaction is provided for slash commands
        reply.replyToInteraction(interaction, "I have joined your voice channel",undefined, duration.ThirtySeconds)
        const connection = joinVoiceChannel({
            // @ts-ignore
            channelId: interaction.member.voice.channel.id,
            // @ts-ignore
            guildId: interaction.guild.id,
            // @ts-ignore
            adapterCreator: interaction.channel.guild.voiceAdapterCreator,
        });
    },
} as ICommand