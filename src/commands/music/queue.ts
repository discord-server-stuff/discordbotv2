import { ICommand } from "wokcommands"
import {duration, reply} from "../../helpers/messages";
import {embeds} from "../../helpers/embeds";
import {distube} from "../../main";
import {VoiceBasedChannel} from "discord.js";
import {channelChecks} from "../../helpers/validationChecks";
import replyToMessage = reply.replyToMessage;
import replyToInteraction = reply.replyToInteraction;

export default {
    names: "queue",
    category: 'Music',
    description: 'Get the queue', // Required for slash commands
    aliases: ['q'],
    //TODO:Make multipage
    slash: 'both', // Create both a slash and legacy command
    testOnly: false, // Only register a slash command for the testing guilds

    callback: ({ message, interaction, args }) => {
        //Something happened causing an error
        if (!channelChecks.voiceChannelChecks(message, interaction)) return;


        // message is provided for a legacy command
        if (message) {
            const queue = distube.getQueue(message)
            if (queue?.songs === undefined){
                replyToMessage(message, embeds.createSimpleWarning("The Queue is empty"),undefined,duration.OneMinute)
                return;
            }
            // @ts-ignore
            const queueText = 'Current queue:\n' + queue.songs.map((song, id) =>
                `**${id+1}**. [${song.name}](${song.url}) - \`${song.formattedDuration}\``
            ).join("\n")
            const queueEmbed =embeds.createSimpleWarning(queueText)
            reply.replyToMessage(message, queueEmbed,undefined, duration.OneMinute, true)
            return
        }
        // interaction is provided for slash commands
        const queue = distube.getQueue(interaction)
        if (queue?.songs === undefined){
            replyToInteraction(interaction, embeds.createSimpleWarning("The Queue is empty"),undefined,duration.OneMinute)
            return;
        }
        // @ts-ignore
        const queueText = 'Current queue:\n' + queue.songs.map((song, id) =>
            `**${id+1}**. [${song.name}](${song.url}) - \`${song.formattedDuration}\``
        ).join("\n")
        const queueEmbed =embeds.createSimpleWarning(queueText)

        reply.replyToInteraction(interaction, queueEmbed,undefined, duration.OneMinute)
    },
} as ICommand

