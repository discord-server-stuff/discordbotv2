import {GuildMember} from "discord.js";
import {DateTimeDifference} from "datetime-difference";
import {v4 as uuidv4} from 'uuid';

export module misc{
    /**
     *
     * @param voiceChannelList list of channels to move between
     * @param currentVoiceChannel current channel in which the user is in
     * @param user the user to be moved
     */
    export async function moveToRandomVoiceChannel(voiceChannelList: string[], currentVoiceChannel: string, user: GuildMember) {
        let newVoiceChannel = currentVoiceChannel;
        while (currentVoiceChannel === newVoiceChannel){
            const random = Math.floor(Math.random() * voiceChannelList.length);
            newVoiceChannel = voiceChannelList[random]
        }
        let newChannel = await user.guild.channels.fetch(newVoiceChannel)
        // @ts-ignore
        await user.voice.setChannel(newChannel)
    }

    /**
     * Used to compute the number of days produced by the DateTimeDifference as it outputs in years, months, days, hours
     * @param dateTimeDifference the difference between two dates
     */
    export function computeNumberOfDays(dateTimeDifference: DateTimeDifference): number {
        let days = 0;
        days += dateTimeDifference.years * 365.25
        days += dateTimeDifference.days
        days += dateTimeDifference.hours / 24
        return days
    }

    /**
     * Generates a UUID
     * @return Returns a string containing a UUID
     */
    export function generateUUID(): string {
        let uuid = uuidv4();

        return uuid
    }

}