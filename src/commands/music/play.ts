import { ICommand } from "wokcommands"
import {deletes, duration, reply, validURL} from "../../helpers/messages";
import {embeds} from "../../helpers/embeds";
import {distube} from "../../main";
import {GuildMember, Message, VoiceBasedChannel, VoiceChannel} from "discord.js";
import {pausing} from "../../helpers/music";
import deleteMessageAfterTime = deletes.deleteMessageAfterTime;
import {channelChecks} from "../../helpers/validationChecks";
import replyToInteraction = reply.replyToInteraction;

export default {
    names: "play",
    category: 'Music',
    description: 'Play used to request a song or resume playing', // Required for slash commands
    aliases: ['p'],
    slash: "both", // Create both a slash and legacy command
    testOnly: false,
    options: [
        {
            name: 'link', // Must be lower case
            description: 'This can be a link, song/album/artist, etc.',
            required: false,
            type: 'STRING',
        },
    ],

    // Only register a slash command for the testing guilds
    callback: ({message, interaction, args}) => {
        if (!channelChecks.voiceChannelChecks(message, interaction)) return;
        if (message) {
            // @ts-ignore
            let voiceChannel = message.member.voice.channel;
            //Check if bot is paused
            if (pausing.isPaused()) {
                pausing.changePauseState();
                distube.resume(message);
                message.react("👍");
                return
            }

            //Check for args
            if (!args[0]) {
                const Embed = embeds.createSimpleIssue("You need to provide a song/playlist/link etc.")
                reply.replyToMessage(message, Embed,undefined, duration.ThirtySeconds)
                return;
            }

            //Handle Spotify links
            if (args.join().includes("open.spotify.com/")) {
                //TODO: Do this
            }

            //Handle everything else3
            const isURL = validURL(args.join());
            const firstSong = true
            if (firstSong && !isURL) {
                distube.search(args.join()).then(searchResult => {
                    // @ts-ignore
                    distube.play(voiceChannel, searchResult[0].url,{textChannel: message.channel})
                })

            } else {
                // @ts-ignore
                distube.play(voiceChannel, args.join(), {textChannel: message.channel});
            }

            deleteMessageAfterTime(message, duration.ThirtySeconds)
            return;
        }

        //Check if bot is paused
        if (pausing.isPaused()) {
            pausing.changePauseState();
            distube.resume(interaction);
            reply.replyToInteraction(interaction, "Media unpaused")
            return
        }

        const [link] = args
        //Check for args
        if (!link) {
            const Embed = embeds.createSimpleIssue("You need to provide a song/playlist/link etc.")
            reply.replyToInteraction(interaction, Embed,undefined, duration.ThirtySeconds)
            return;
        }
        //Handle Spotify links
       /* if (link.includes("open.spotify.com/")) {
            return
        }*/

        //Handle everything else3
        const isURL = validURL(link);
        console.log(link)
        const firstSong = true
        // @ts-ignore
        let voiceChannel = interaction.member.voice.channel;

        if(firstSong && !isURL){

            distube.search(link).then(searchResult => {
                // @ts-ignore
                distube.play(voiceChannel, searchResult[0].url, {textChannel: interaction.channel}).then(() => {
                    replyToInteraction(interaction, embeds.createSimpleWarning("Done diego"),undefined, duration.ThirtySeconds)
                }).catch(error =>{
                    reply.replyToInteraction(interaction, embeds.createErrorEmbed("Something went wrong",error))
                    return
                })
            })

        } else {
            // @ts-ignore
            distube.play(voiceChannel, link, {textChannel: interaction.channel}).then( () => {
                replyToInteraction(interaction, embeds.createSimpleWarning("Done diego"),undefined, duration.ThirtySeconds)
            }).catch(error =>{
                reply.replyToInteraction(interaction, embeds.createErrorEmbed("Something went wrong",error))
                return
            })

        }

    },
} as ICommand

