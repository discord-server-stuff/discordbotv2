# syntax=docker/dockerfile:1
FROM nikolaik/python-nodejs
WORKDIR /
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update
RUN apt-get install -y ffmpeg

# Install node dependencies
COPY package*.json ./
RUN npm install -g npm
RUN npm install
RUN npm install @distube/ytdl-core@latest
# install python dependencies
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY . .
CMD [ "npm", "start"]