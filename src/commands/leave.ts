import { ICommand } from "wokcommands"
import {deletes, duration, reply} from "../helpers/messages";
import {embeds} from "../helpers/embeds";
import {getVoiceConnection} from "@discordjs/voice";
import {distube} from "../main";
import {pausing} from "../helpers/music";
import isPaused = pausing.isPaused;
import {channelChecks} from "../helpers/validationChecks";
import deleteMessageAfterTime = deletes.deleteMessageAfterTime;
import replyToInteraction = reply.replyToInteraction;

export default {
    names: "leave",
    category: 'General',
    description: 'Leave the voice channel',

    slash: 'both', // Create both a slash and legacy command
    testOnly: false, // Only register a slash command for the testing guilds

    callback: ({ message, interaction, client}) => {

        const Embed = embeds.createSimpleIssue("You need to be in the same channel as the bot to use this command")
        if (!channelChecks.voiceChannelChecks(message, interaction)) return;
        //Handle legacy command
        if (message) {
            // @ts-ignore
            const connection = getVoiceConnection(message.guild.id)
            if (!connection) return;
            //Check same channel
            // @ts-ignore
            const inSameChannel = message.member.voice.channel.id === message.guild.me.voice.channel.id
            if (!inSameChannel){
                reply.replyToMessage(message, Embed, undefined, duration.ThirtySeconds)
                return;
            }
            // @ts-ignore
            distube.voices.get(message)?.leave();
            message.react("👋")
            deleteMessageAfterTime(message, duration.ThirtySeconds)
            return
        }
        //Interaction is provided for slash commands
        // @ts-ignore
        const connection = getVoiceConnection(interaction.guild.id)
        if (!connection) return;
        //Check same channel
        // @ts-ignore
        const inSameChannel = message.member.voice.channel.id === message.guild.me.voice.channel.id
        if (!inSameChannel){
            reply.replyToMessage(message, Embed, undefined, duration.ThirtySeconds)
            return;
        }

        replyToInteraction(interaction,embeds.createSimpleInfoEmbed("I'm leaving", "bye"),undefined,duration.TenSeconds)
        // @ts-ignore
        distube.voices.get(interaction)?.leave();
    },
} as ICommand