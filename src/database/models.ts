import mongoose from "mongoose";

/**
 * Different schemas used to interact with the mongoDB
 */
export module models {
    /**
     * A simple schema used for testing
     */
    export module testModel {
        /**
         * Interface used for the schema
         */
        export interface ITest extends Document {
            message: string;
        }

        /**
         * Just a schema for testing that takes a required input string message
         */
        export const testSchema = new mongoose.Schema<ITest>({
            message: {
                type: String,
                required: true
            }
        });

        /**
         * Model for the schema
         */
        export const TestModel = mongoose.model<ITest>("test", testSchema);
    }
    /**
     * Used to store information about accounts
     */
    export module accountModel {
        /**
         * Interface used for the schema
         */
        export interface IAccount extends Document {
            /** The account username */
            username: string;
            /** The account password */
            password: string;
            /** The game which this account is ued for */
            game: string;
            /** The unique id of this account */
            databaseId: string;
            /** The Discord user who last edited/created the account */
            lastEditedBy: string;
        }

        /**
         * A schema used to store account data
         */
        export const accountSchema = new mongoose.Schema<IAccount>({
            username: {
                type: String,
                required: true
            },
            password: {
                type: String,
                required: true
            },
            game: {
                type: String,
                required: true
            },
            databaseId: {
                type: String,
                required: true
            },
            lastEditedBy: {
                type: String,
                required: true
            }
        });

        /**
         * Model for the schema
         */
        export const AccountModel = mongoose.model<IAccount>("account", accountSchema);
    }



}