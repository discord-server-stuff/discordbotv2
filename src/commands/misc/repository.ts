import { ICommand } from "wokcommands"
import {duration, reply} from "../../helpers/messages";
import {embeds} from "../../helpers/embeds";

export default {
    names: "repository",
    category: 'general',
    description: 'Used to get the git repository for the bot', // Required for slash commands
    aliases: ['repo','git'],

    slash: 'both', // Create both a slash and legacy command
    testOnly: false, // Only register a slash command for the testing guilds
    ownerOnly: false,

    callback: ({ message, interaction, client}) => {
        const replyMsg = embeds.createSimpleInfoEmbed("Git Repository","https://gitlab.com/discord-server-stuff")
            .setThumbnail("https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png")
            .setFooter({
                text: "",
                iconURL: "https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png"
            })
        // message is provided for a legacy command
        console.log(client)
        if (message) {
            reply.replyToMessage(message, replyMsg,undefined, duration.ThirtySeconds, true)
            return
        }
        // interaction is provided for slash commands
        reply.replyToInteraction(interaction, replyMsg,undefined, duration.ThirtySeconds)
    },
} as ICommand