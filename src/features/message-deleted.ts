import {Client, Message, TextChannel} from 'discord.js'
import WOKCommands from 'wokcommands'
import {deletes} from "../helpers/messages";

export default (client: Client, instance: WOKCommands) => {
    // Listen for deletes
    client.on('messageDelete', (message) => {
        if (!deletes.isMessageDeleted(<Message>message)){
            deletes.markMessageAsDeleted(<Message>message)
        }
    })
}

const config = {
    // The display name that server owners will see.
    // This can be changed at any time.
    displayName: 'Message Deleted',

    // The name the database will use to set if it is enabled or not.
    // This should NEVER be changed once set, and users cannot see it.
    dbName: 'Message Deleted'
}

export { config }
