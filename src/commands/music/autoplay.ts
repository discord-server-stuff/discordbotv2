import { ICommand } from "wokcommands"
import {duration, reply} from "../../helpers/messages";
import {embeds} from "../../helpers/embeds";
import {distube} from "../../main";
import {channelChecks} from "../../helpers/validationChecks";

export default {
    names: "autoplay",
    category: 'Music',
    description: 'Toggle autoplay mode, which automatically finds related songs', // Required for slash commands
    slash: 'both', // Create both a slash and legacy command
    testOnly: false, // Only register a slash command for the testing guilds

    callback: ({ message, interaction }) => {
        if (!channelChecks.voiceChannelChecks(message, interaction)) return;

        // message is provided for a legacy command
        if (message) {
            // Check queue
            if (distube.getQueue(message) == undefined){
                reply.replyToMessage(message, embeds.createErrorEmbed("Nothing is playing"),undefined, duration.ThirtySeconds,true)
                return;
            }

            try{
                const mode = distube.toggleAutoplay(message);
                const returnEmbed = embeds.createSimpleInfoEmbed("Autoplay","Set autoplay mode to `" + (mode ? "On" : "Off") + "`")
                reply.replyToMessage(message,returnEmbed,undefined,duration.OneMinute,true)
            }
            catch(err){
                reply.replyToMessage(message, embeds.createErrorEmbed("Something went wrong"),undefined, duration.OneMinute, true)
            }
            return
        }
        // interaction is provided for slash commands
        // Check queue
        if (distube.getQueue(interaction) == undefined){
            reply.replyToInteraction(interaction, embeds.createErrorEmbed("Nothing is playing"),undefined, duration.ThirtySeconds)
            return;
        }

        try{
            const mode = distube.toggleAutoplay(interaction);
            const returnEmbed = embeds.createSimpleInfoEmbed("Autoplay","Set autoplay mode to `" + (mode ? "On" : "Off") + "`")
            reply.replyToInteraction(interaction, returnEmbed,undefined,duration.OneMinute)
        }
        catch(err){
            reply.replyToInteraction(interaction, embeds.createErrorEmbed("Something went wrong"),undefined, duration.OneMinute)
        }

    },
} as ICommand

