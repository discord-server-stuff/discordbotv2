import { ICommand } from "wokcommands"
import {duration, reply} from "../../helpers/messages";
import {embeds} from "../../helpers/embeds";
import {distube} from "../../main";
import {VoiceBasedChannel} from "discord.js";
import {channelChecks} from "../../helpers/validationChecks";

export default {
    names: "Goto",
    category: 'Music',
    description: 'Goto to the song number in the queue', // Required for slash commands
    aliases: ['jump'],
    minArgs: 1,
    maxArgs: 1,
    expectedArgs: '<number>',
    slash: 'both', // Create both a slash and legacy command
    testOnly: false, // Only register a slash command for the testing guilds
    options: [
        {
            name: 'position', // Must be lower case
            description: 'Song you want to play in the queue',
            required: true,
            type: 'NUMBER',
        },
    ],

    callback: ({ message, interaction, args }) => {
        if (!channelChecks.voiceChannelChecks(message, interaction)) return;

        const errorEmbed = embeds.createErrorEmbed("Invalid song number.")


        // message is provided for a legacy command
        if (message) {
            if(distube.getQueue(message)?.songs !== undefined){
                // @ts-ignore
                if(distube.getQueue(message).songs.length < 2){
                    const errorEmbed = embeds.createErrorEmbed("Not enough songs to do a skip")
                    reply.replyToMessage(message, errorEmbed,undefined, duration.OneMinute, true)
                    return;
                }
            }

            try{
                message.react("👌")
                distube.jump(message, parseInt(args[0]))
            }
            catch(err){
                reply.replyToMessage(message, errorEmbed,undefined, duration.OneMinute, true)
            }

            return
        }
        // interaction is provided for slash commands

        if(distube.getQueue(interaction)?.songs !== undefined){
            // @ts-ignore
            if(distube.getQueue(interaction).songs.length < 2){
                const errorEmbed = embeds.createErrorEmbed("Not enough songs to skip")
                reply.replyToInteraction(interaction, errorEmbed,undefined, duration.OneMinute)
            }
        }

        try{
            distube.jump(interaction, parseInt(args[0]))
            reply.replyToInteraction(interaction, embeds.createSimpleWarning("Skipping to song " + args[0]),undefined, duration.TenSeconds)
        }
        catch(err){
            reply.replyToInteraction(interaction, errorEmbed,undefined, duration.OneMinute)
        }

    },
} as ICommand

