import { ICommand } from "wokcommands"
import {duration, reply} from "../../helpers/messages";
import {embeds} from "../../helpers/embeds";
import {distube} from "../../main";
import datetimeDifference from "datetime-difference";
import {misc} from "../../helpers/misc";

export default {
    names: "countdown",
    category: 'Memes',
    description: 'Tid til at kæresten kan blive hentet i bilen når hun skal hjem fra folkeskolen',

    slash: 'both', // Create both a slash and legacy command
    testOnly: false, // Only register a slash command for the testing guilds

    callback: ({ message, interaction }) => {
        let timeLeft = datetimeDifference(new Date(), new Date(2025,3,18))

        // Prettify the output of DateTimeDifference
        const readme = Object.keys(timeLeft)
            // @ts-ignore
            .map(k => `${ timeLeft[k] } ${ k }`)
            .join(", ");
        const replyMsg = embeds.createSimpleInfoEmbed("Træ","Næste fældning: " + readme)

        let daysLeft = misc.computeNumberOfDays(timeLeft)
        // message is provided for a legacy
        if (message) {
            if (daysLeft <= 1){
                // @ts-ignore
                let voiceChannel = message.member.voice.channel;
                if (distube.getQueue(message) != undefined ){
                    // @ts-ignore
                    let numberOfSongs = distube.getQueue(message).songs.length()
                    // @ts-ignore
                    distube.play(voiceChannel,"https://www.youtube.com/watch?v=_JgbyISzmjQ", {position: numberOfSongs - 1}).then(() => {
                        reply.replyToMessage(message, replyMsg,undefined, duration.OneMinute, true)
                    })
                    return
                }
                // @ts-ignore
                distube.play(voiceChannel,"https://www.youtube.com/watch?v=_JgbyISzmjQ").then(() => {
                    reply.replyToMessage(message, replyMsg,undefined, duration.OneMinute, true)
                })
                return
            }
            reply.replyToMessage(message, replyMsg,undefined, duration.OneMinute, true)
            return
        }
        // interaction is provided for slash commands
        if (daysLeft <= 1){
            // @ts-ignore
            let voiceChannel = interaction.member.voice.channel;
            if (distube.getQueue(interaction) != undefined ){
                // @ts-ignore
                let numberOfSongs = distube.getQueue(interaction).songs.length()
                // @ts-ignore
                distube.play(voiceChannel,"https://www.youtube.com/watch?v=_JgbyISzmjQ", {position: numberOfSongs - 1}).then(() => {
                    reply.replyToInteraction(interaction, replyMsg,undefined, duration.OneMinute)
                })
                return
            }
            // @ts-ignore
            distube.play(voiceChannel,"https://www.youtube.com/watch?v=_JgbyISzmjQ").then(() => {
                reply.replyToInteraction(interaction, replyMsg,undefined, duration.OneMinute)
            })
            return
        }
        reply.replyToInteraction(interaction, replyMsg,undefined, duration.OneMinute)
    },
} as ICommand