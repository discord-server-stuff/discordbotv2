# Discord Bot

### Features
* Music player
    * Spotify(Coverting to Youtube)
    * Youtube
    * Soundcloud
* Bit of random stuff

### Project features
* Containerized 
* Pipeline integration
* In conjunction with other project, this discord bot can be monitored



### Todo
* Learn TS better so i dont have to use the @ts-ignore  :>
* Ai meme generator
* AI reptile brain
* Event creation
* Spotify Support
* Setup Pages CI/CD on GITLAB for documentation
* Create a prometheus exporter for the discord bot which should expose the following metrics:
  * Frequency of which commands is callled
  * What songs are played
  * Who plays the most music

##Notes
* docker build --tag kjellefar/discord_bot:dev .
* docker push kjellefar/discord_bot:dev