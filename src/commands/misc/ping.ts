import { ICommand } from "wokcommands"
import {duration, reply} from "../../helpers/messages";
import {embeds} from "../../helpers/embeds";

export default {
    names: "Ping",
    category: 'Fun & Games',
    description: 'Replies with pong', // Required for slash commands

    slash: 'both', // Create both a slash and legacy command
    testOnly: false, // Only register a slash command for the testing guilds

    callback: ({ message, interaction }) => {
        const replyMsg = embeds.createSimpleInfoEmbed("Ping!","Legacy reply Ping!")
        // message is provided for a legacy command
        if (message) {
            reply.replyToMessage(message, replyMsg,undefined, duration.TenSeconds, true)
            return
        }
        // interaction is provided for slash commands
        reply.replyToInteraction(interaction, replyMsg,undefined, duration.TenSeconds)
    },
} as ICommand