import { ICommand } from "wokcommands"
import {duration, reply} from "../../helpers/messages";
import {embeds} from "../../helpers/embeds";
import Discord from "discord.js";

export default {
    names: "testing",
    category: 'Testing',
    description: 'Used for testing', // Required for slash commands
    aliases: ['t'],

    slash: 'both', // Create both a slash and legacy command
    testOnly: true, // Only register a slash command for the testing guilds
    ownerOnly: true,

    callback: async ({ message, interaction }) => {
        let msg: Discord.Message
        const replyMsg = embeds.createSimpleInfoEmbed("Ping!","Legacy reply Ping!")
        // message is provided for a legacy command
        if (message) {
            msg = await reply.replyToMessage(message, replyMsg, undefined, duration.TenSeconds, true)
            msg.react("😊")
            return
        }
        // interaction is provided for slash commands
        reply.replyToInteraction(interaction, replyMsg,undefined, duration.TenSeconds)
    },
} as ICommand